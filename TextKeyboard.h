#pragma once
#include "Arduino.h"

class TextKeyboard
{
	private:
		String text;
		byte limit;

		unsigned int time;

		bool ABC;
		byte caret;

		byte last_id;
		byte key_iter;
		bool max_iter;

		bool sealed;

	public:
		TextKeyboard();
		~TextKeyboard();

		void react(byte key);
		void setText(String txt, byte charlimit = 255);
		void clearText();

		bool isLiteralModeOn();
		String getText();
		byte getLimit();
		byte getCaretPos();
	
	private:
		void append(char c);
		void append(String str);
		void erase();
		void changeLast(char c);
};