#include "ModeStandard.h"

ModeStandard::ModeStandard(Adafruit_SSD1306* display, SdFat* SD)
	: Mode(display, SD)
{
	Serial.println("MODE: Standard");

	result = String(0);
	memory = 0;
	operator_id = 0;
	operand = 0;
	operand_b = false;
	result_b = false;

	exp = false;
	error = false;

	byte tmp[24] = 
	{
		7,8,9,10,
		4,5,6,11,
		1,2,3,12,
		16,0,15,17,

		19,20,21,22,23,13,14,18
	};

	memcpy(keyMap, tmp, 24);

	readSettings(SD, settings);
	code_indicator = 0;

	dsp->setTextSize(2);
	this->display(result);
}

ModeStandard::~ModeStandard()
{

}

void ModeStandard::display(String str)
{
	dsp->clearDisplay();

	String res = "          ";

	if (str.toDouble() < 0)
	{
		str.remove(str.indexOf('-'), 1);
		res[0] = '-';
	}

	// MEMORY indicator
	if (memory != 0)
	{
		dsp->setCursor(0, 0);
		dsp->print("M");
	}

	// ERROR indicator
	if (error)
	{
		dsp->setCursor(0, 0);
		dsp->print("Err");
	}

	int dotindex = str.indexOf('.');
	String integer = str.substring(0, (dotindex == -1) ? str.length() : dotindex);

	// Digit limit exceeded:
	if (integer.length() > 8)
	{
		// This is done because Err sign interferes with E sign
		if (!error)
		{
			dsp->setCursor(108, 0);
			dsp->print("E");
		}

		exp = true;
		int newdot = integer.length() - 8;

		str = integer.substring(0, newdot);
		str += '.';
		str += integer.substring(newdot);

		dotindex = str.indexOf('.');
	}

	// Cut to fit the digit limit:
	str = str.substring(0, 9);

	if (dotindex == -1)
		str.remove(8, 1);

	String tmp = str;

	if (res[0] == '-')
		str = '-' + str;

	if(dotindex != -1)
		tmp.remove(tmp.indexOf('.'), 1);

	int iter = 9;

	for (int i = tmp.length() - 1; i >= 0; i--)
	{
		if (i == dotindex - 1)
			res[iter--] = '.';
		
		res[iter--] = tmp[i];
	}

	if (exp)
	{
		this->result = str;
	}

	dsp->setCursor(0, 16);
	dsp->println(res);
	dsp->display();
}

f64 ModeStandard::convert(String str)
{
	int dotpos = str.indexOf('.');
	
	if (dotpos == -1)
		return f64(str.toInt());

	String s1 = str.substring(0, dotpos);
	String s2 = str.substring(dotpos + 1);
	f64 fraction = s2.toInt();

	for (int i = 0; i < s2.length(); i++)
		fraction /= 10;
	
	f64 integer = s1.toInt();
	
	return integer + fraction;
}

String ModeStandard::convert(f64 num)
{
	String res = num.toString(7);

	if (num.floor() == num)
		res.remove(res.indexOf('.'));

	return res;
}

f64 ModeStandard::calculate(f64 operand1, f64 operand2, int operator_id)
{
	if (operator_id == 11)
	{
		return operand1 - operand2;
	}
	else if (operator_id == 12)
	{
		return operand1 + operand2;
	}
	else if (operator_id == 13)
	{
		if (operand2 == 0)
		{
			error = true;
			return f64(0);
		}

		return  operand1 / operand2;
	}
	else if (operator_id == 14)
	{
		return operand1 * operand2;
	}
	else if (operator_id == 18)
	{
		if (operand2 < 0)
		{
			error = true;
			return f64(0);
		}

		return operand2.sqrt();
	}
}

MODE ModeStandard::react(byte id)
{
	if (id < 0)
		return M_STANDARD;

	// Error notification block:
	if (error)
	{
		if (id == 23)
			error = false;
		else
			return M_STANDARD;
	}

	// Searching for the access code pattern:
	if (id == settings.keycode[code_indicator])
	{
		code_indicator++;
		
		if (code_indicator == 6)
			return M_MENU;
	}
	// If another plus was pressed at the beginning of sequence - do nothing.
	else if (code_indicator == 2 && id == 12);
	// Else, go back to the beginning.
	else 
		code_indicator = 0;


	// EXPONENTIAL notification block
	if (exp && !(id == 17 || id == 23))
		return M_STANDARD;
	
	// NUMBER key pressed:

	if (id >= 0 && id <= 9)
	{
		result_b = true;

		if (result.indexOf('.') == -1)
		{
			if (result[0] == '0' || (result[0] == '-' && result[2] == '0'))
				result.remove(result.indexOf('0'), 1);

			if(result.length() < 8)
				result += String(id);
		}
		else
		{
			if (result.length() < 9)
				result += String(id);
		}
	}

	// PERCENT key  pressed:

	else if (id == 10)
	{
		if (operator_id)
		{
			if (operator_id == 11)
				result = convert(calculate(operand, ((f64) 1 - convert(result) / (f64) 100), 14));
			else if (operator_id == 12)
				result = convert(calculate(operand, ((f64) 1 + convert(result) / (f64) 100), 14));
			else if (operator_id == 13)
				result = convert(calculate(operand, ((f64) 100 / convert(result)), 14));
			else if (operator_id == 14)
				result = convert(calculate(operand, (convert(result) / (f64) 100), 14));

			operator_id = 0;
			operand = 0;
			operand_b = false;
			
			removeRedundant(result);
		}
	}

	// OPERATOR key pressed:

	else if (id >= 11 && id <= 14)
	{
		if (operator_id && operand_b && result_b)
		{
			operand = calculate(operand, convert(result), operator_id);

			String oper = convert(operand);
			removeRedundant(oper);
			display(oper);

			operator_id = id;

			result_b = false;
			result = String(0);

			return M_STANDARD;
		}
		else if (operator_id)
		{
			operator_id = id;

			return M_STANDARD;
		}
		else
		{
			operator_id = id;
			operand = convert(result);
			operand_b = true;

			String oper = convert(operand);
			removeRedundant(oper);
			display(oper);

			result_b = false;
			result = String(0);

			return M_STANDARD;
		}
	}

	else if (id >= 15 && id <= 18)
	{
		// SIGN key pressed:
		if (id == 15)
		{
			if (result[0] == '-')
				result.remove(0, 1);
			else
				result = '-' + result;
		}

		// DOT key pressed:
		else if (id == 16)
		{
			if (result.indexOf('.') == -1 && result.length() < 8)
				result += '.';
		}

		// EQUALS key pressed:
		else if (id == 17)
		{
			if (exp)
			{
				exp = false;
			}
			else if (operator_id)
			{
				result = convert(calculate(operand, convert(result), operator_id));
				removeRedundant(result);

				operator_id = 0;
				operand = 0;
				operand_b = false;
			}
			else
				removeRedundant(result);
		}

		// SQRT key pressed:
		else if (id == 18)
		{
			result = convert(calculate(2, convert(result), 18));
			removeRedundant(result);
		}
	}

	// MEMORY key pressed:
	else if (id >= 19 && id <= 23)
	{
		// M+ key pressed:
		if (id == 19)
			memory += convert(result);
		// M- key pressed:
		else if (id == 20)
			memory -= convert(result);
		// MR key pressed:
		else if (id == 21)
		{
			result = convert(memory);
			result_b = true;
		}
		// MC key pressed:
		else if (id == 22)
			memory = 0;
		// C key pressed:
		else if (id == 23)
		{
			operator_id = 0;
			operand = 0;
			operand_b = false;
			exp = false;
			result = String(0);
		}
	}

	display(result);
	return M_STANDARD;
}

void ModeStandard::removeRedundant(String& str)
{
	int dotindex = str.indexOf('.');

	if (dotindex == -1)
		return;

	while (str.endsWith("0") || str.endsWith("."))
		str.remove(str.length() - 1, 1);
}