#pragma once

class DecimalNumber
{
	public:
		double number;
		int decimal;
		
		DecimalNumber(double n);
		~DecimalNumber();
	
	DecimalNumber operator+(DecimalNumber a) const;
	DecimalNumber operator*(DecimalNumber a) const;

  DecimalNumber DecimalNumber::operator-(DecimalNumber a) const;
  DecimalNumber DecimalNumber::operator/(DecimalNumber a) const;
  
  bool DecimalNumber::operator==(DecimalNumber a) const;
  bool DecimalNumber::operator<(DecimalNumber a) const;
  bool DecimalNumber::operator>(DecimalNumber a) const;
  bool DecimalNumber::operator<=(DecimalNumber a) const;
  bool DecimalNumber::operator>=(DecimalNumber a) const;

  DecimalNumber& DecimalNumber::operator=(DecimalNumber& n);
  DecimalNumber& DecimalNumber::operator=(double& n);
  
  double DecimalNumber::toDouble();
};
