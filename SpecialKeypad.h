#pragma once

class SpecialKeypad
{
	private:
		struct Key
		{
			String key;
			int ID;
		};

		struct Pin
		{
			byte pin;
			Key key1;
			Key key2;
		};

		static const unsigned int KEY_COUNT = 9;

		static const unsigned int uncertainty = 20;

		static const unsigned int HIGH_VALUE = 970;
		static const unsigned int LOW_VALUE = 90;

		const unsigned int PIN_COUNT;
		byte PowerPin;
		Pin* pins;

	public:
		SpecialKeypad(byte input[], unsigned int pincount)
			: PIN_COUNT(pincount)
		{
			Key keypad[KEY_COUNT];
			pins = new Pin[PIN_COUNT];

			keypad[0].key = "M+";
			keypad[0].ID = 0;

			keypad[1].key = "M-";
			keypad[1].ID = 1;

			keypad[2].key = "MRC";
			keypad[2].ID = 2;

			keypad[3].key = "MC";
			keypad[3].ID = 3;

			keypad[4].key = "C";
			keypad[4].ID = 4;

			keypad[5].key = "/";
			keypad[5].ID = 5;

			keypad[6].key = "x";
			keypad[6].ID = 6;

			keypad[7].key = "sqrt";
			keypad[7].ID = 7;

			/*for (int i = 0; i < PIN_COUNT; i++)
			{
				pins[i].pin = input[i];
				pins[i].key1 = keypad[2 * i];
				pins[i].key2 = keypad[2 * i + 1];
			}*/
		}

		~SpecialKeypad()
		{
			delete[] pins;
		}

		int getKey()
		{
			/*for (int i = 0; i < PIN_COUNT; i++)
			{
				int read = analogRead(pins[i].pin);

				if (read == 0)
					continue;

				if(abs(read - LOW_VALUE) < uncertainty)
					return pins[i].key1.ID;
				else if(abs(read - HIGH_VALUE) < uncertainty)
					return pins[i].key2.ID;
			}
			*/
			return -1;
		}
};