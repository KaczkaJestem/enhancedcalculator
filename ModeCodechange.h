#pragma once
#include "Mode.h"
#include "FileManagement.h"

class ModeCodechange : public Mode
{
private:
	byte keyMap[24];
	byte code[4];

public:
	ModeCodechange(Adafruit_SSD1306* display, SdFat* SD);
	~ModeCodechange();

	virtual MODE react(byte id);

	void display();
};
