#include "ModeCodechange.h"

ModeCodechange::ModeCodechange(Adafruit_SSD1306* display, SdFat* SD)
	: Mode(display, SD)
{
	Serial.println("MODE: Codechange");

	for (int i = 0; i < 4; i++)
		code[i] = 255;

	this->display();
}

ModeCodechange::~ModeCodechange()
{

}

MODE ModeCodechange::react(byte id)
{
	if (id >= 0 && id < 10)
	{
		for (int i = 0; i < 4; i++)
			if (code[i] == 255)
			{
				code[i] = id;
				break;
			}
	}

	else if (id == 10)
	{
		for (int i = 0; i < 4; i++)
			if (i == 3 || code[i + 1] == 255)
				code[i] = 255;
	}

	else if (id == 23)
		return M_MENU;

	else if (id == 17 && code[3] != 255)
	{
		Settings settings;
		if (!readSettings(sd, settings))
		{
			return M_MENU;
		}

		settings.keycode[2] = code[0];
		settings.keycode[3] = code[1];
		settings.keycode[4] = code[2];
		settings.keycode[5] = code[3];

		saveSettings(sd, settings);

		return M_MENU;
	}

	this->display();
	return M_CODECHANGE;
}

void ModeCodechange::display()
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->setTextSize(1);
	dsp->println("Enter new keycode:");
	dsp->println("");

	dsp->setTextSize(2);

	for (int i = 0; i < 4; i++)
	{
		if (code[i] == 255)
			dsp->print('*');
		else
			dsp->print(code[i]);
	}
	
	dsp->println("");
	dsp->setTextSize(1);
	
	dsp->println("'=' - accept");
	dsp->println("'%' - erase");
	dsp->println("'C' - cancel");

	dsp->display();
}