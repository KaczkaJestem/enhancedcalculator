#pragma once
#include "Arduino.h"
#include <Adafruit_SSD1306.h>
#include <SdFat.h>

enum MODE
{
	M_STANDARD,

	M_PLOTS,
	M_NOTES,
	M_SNAKE,
	M_CHAT,
	M_CODECHANGE,

	M_MENU
};

class Mode
{
	protected:
		Adafruit_SSD1306* dsp;
		SdFat* sd;

	public:
		Mode(Adafruit_SSD1306* display, SdFat* SD);
		~Mode();

		virtual MODE react(byte id) = 0;
		virtual void update();
};