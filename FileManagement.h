#pragma once

#include <SdFat.h>

struct Settings
{
	byte keycode[6];
	char username[10];
};

bool generateSettingsFile(SdFat* SD);
bool readSettings(SdFat* SD, Settings& settings);
bool saveSettings(SdFat* SD, Settings& settings);

bool logFromChat(SdFat* SD, String line);
bool readChatLog(SdFat* SD, String& dest, byte size, uint16_t line_offset);
bool clearChatLog(SdFat* SD);
bool saveChatLogToNotes(SdFat* SD, int& savefileindex);