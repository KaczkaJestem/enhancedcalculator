#include "FileManagement.h"
#include "Arduino.h"

bool generateSettingsFile(SdFat* SD)
{
	File file = SD->open("settings.s", FILE_WRITE);

	if (!file)
	{
		Serial.println("Cannot create settings file! Only standard mode will be available.");
		file.close();

		return false;
	}

	file.truncate(0);

	byte keycode[6] = { 12, 12, 1, 2, 3, 4 };
	file.write(keycode, 6);

	char username[11] = "username";
	file.write(username, sizeof(char) * 11);

	file.close();
	return true;
}

bool readSettings(SdFat* SD, Settings& settings)
{
	File file = SD->open("settings.s", FILE_READ);

	if (!file)
	{
		generateSettingsFile(SD);
		file = SD->open("settings.s", FILE_READ);
	}

	if (!file)
	{
		Serial.println("Cannot read settings file! Only standard mode will be available.");
		file.close();
		return false;
	}
	else
	{
		file.read(settings.keycode, 6);
		file.read(settings.username, sizeof(char) * 11);
	}

	file.close();
	return true;
}

bool saveSettings(SdFat* SD, Settings& settings)
{
	File file = SD->open("settings.s", FILE_WRITE);

	if (!file)
	{
		Serial.println("Cannot create settings file! Only standard mode will be available.");
		file.close();
		return false;
	}

	file.truncate(0);

	file.write(settings.keycode, 6);
	file.write(settings.username, sizeof(char) * 11);

	file.close();
	return true;
}

bool clearChatLog(SdFat* SD)
{
	File log = SD->open("chat/chatlog.txt", FILE_WRITE);

	if (!log)
	{
		log.close();
		return false;
	}
	
	log.truncate(0);
	log.close();
	
	return true;
}

bool logFromChat(SdFat* SD, String line)
{
	File log = SD->open("chat/chatlog.txt", FILE_WRITE);

	if (!log)
	{
		Serial.println("Cannot access log file! Problems with chat may be encountered.");
		log.close();
		return false;
	}

	while (line.length() % 21 != 0)
		line += ' ';

	log.write(line.c_str(), sizeof(char) * line.length());
		
	log.close();
	return true;
}

bool readChatLog(SdFat* SD, String& dest, byte size, uint16_t line_offset)
{
	File log = SD->open("chat/chatlog.txt", FILE_READ);

	if (!log)
	{
		// If file not present, try to create one:

		log = SD->open("chat/chatlog.txt", FILE_WRITE);
		log.close();
		log = SD->open("chat/chatlog.txt", FILE_READ);
	}

	if (!log)
	{
		Serial.println("Cannot read log file! Problems with chat may be encountered.");
		log.close();
		return false;
	}

	long startpos = (log.fileSize() / sizeof(char)) - size - line_offset * 21;

	if (startpos < 0)
		startpos = 0;

	log.seek(startpos);

	dest = "";
	char c;

	for (int i = 0; i < size; i++)
	{
		int res = 0;
		
		if (log.available() != 0)
		{
			res = log.read(&c, sizeof(char));

			if (res == -1)
				return false;

			dest += c;
		}
		else
			dest += ' ';
	}

	log.close();
	return true;
}

bool saveChatLogToNotes(SdFat* SD, int& savefileindex)
{
	File file;
	int counter = savefileindex;
	String filename;

	// IF FILENAME INDEX WAS NOT SPECIFIED:
	if (savefileindex == -1)
	{
		do
		{
			counter++;
			filename = "notes/chat" + String(counter) + ".note";
		}
		while (SD->exists(filename.c_str()));

		savefileindex = counter;
	}
	// IF FILENAME INDEX WAS SPECIFIED:
	else
	{
		filename = "notes/chat" + String(counter) + ".note";
		file = SD->open(filename, FILE_WRITE);

		if (!file)
		{
			file.close();
			return false;
		}

		file.truncate(0);
		file.close();
	}

	file = SD->open("chat/chatlog.txt", FILE_READ);

	if (!file)
	{
		Serial.println("Error while saving file!");
		file.close();
		return false;
	}

	char buff[126];
	int pos = 0;
	
	while (file.available())
	{
		int ret = file.read(&buff, sizeof(char) * 126);
		pos = file.curPosition();

		file.close();
		file = SD->open(filename, FILE_WRITE);
		if (!file) return false;

		for (int i = 0; i < 126; i++)
		{
			if (i < ret)
			{
				file.write(&buff[i], sizeof(char));
				file.write((uint8_t) 0);
			}
			else
			{
				file.write(' ');
				file.write((uint8_t) 0);
			}
		}

		file.close();
		file = SD->open("chat/chatlog.txt", FILE_READ);
		if (!file) return false;
		file.seekSet(pos);
	}

	file.close();
	return true;
}