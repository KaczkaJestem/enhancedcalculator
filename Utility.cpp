#include "Utility.h"

int getFreeRAM()
{
	 extern int __heap_start, *__brkval;
	 int v;
	 return (int)&v - (__brkval == 0 ? (int)&__heap_start : (int)__brkval);
}

String readESP()
{
	long now, time = millis();
	

	while (true) {
		if (Serial1.available())
		{
			String reading = Serial1.readString();
			reading.trim();
			return reading;
		}

		now = millis();
		if (now - time > 10000)
			break;
	}


	return "err_to";
}

void handleESPMessage(SdFat* sd)
{
	String reading = Serial1.readString();
	reading.trim();

	if (!ModeChat::session.available)
		return;

	int split = reading.indexOf(' ');
	String cmd = reading.substring(0, split);

	if (cmd == F("msg"))
	{
		String msg = reading.substring(split + 1);
		msg.replace('_', ' ');

		logFromChat(sd, msg);

		ModeChat::session.lines_count += ceil((double)msg.length() / 21.0);
	}
	else if (cmd == F("cmd_cc"))
	{
		ModeChat::session.client_count = reading.substring(split + 1).toInt();
	}
	else if (cmd == F("err_disc"))
	{
		ModeChat::session.client_count = 0;
		ModeChat::session.available = false;
	}
}