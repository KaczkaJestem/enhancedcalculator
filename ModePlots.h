#pragma once
#include "Mode.h"
#include "TextKeyboard.h"
#include "Float64.h"
#include "parser\tinyexpr.h"

class ModePlots : public Mode
{
private:
	enum SUBMODE{
		SM_ENTER,
		SM_PLOT,
		SM_RESULT
	};

	byte keyMap[24];
	
	TextKeyboard txt_kbd;

	unsigned int time_blinker;
	bool blinker;

	te_expr *expr;
	double* x;
	int err;
	
	double step;
	double speed;
	double pos_x, pos_y;
	byte cur_pos_x, cur_pos_y;
	bool cursor;

	SUBMODE mode;

public:
	ModePlots(Adafruit_SSD1306* display, SdFat* SD);
	~ModePlots();

	virtual MODE react(byte id);
	virtual void update();

	void displayMenu();
	void displayPlot();

private:
	void calculateExpr();
	void plotExpr();
};