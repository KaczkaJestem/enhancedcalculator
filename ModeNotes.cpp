#include "ModeNotes.h"
#include "CustomChars.h"

ModeNotes::ModeNotes(Adafruit_SSD1306* display, SdFat* SD)
	: Mode(display, SD)
{
	Serial.println("MODE: Notes");

	mode = SM_BROWSER;
	setupBrowser();

	this->display_browser();
}

ModeNotes::~ModeNotes()
{
	folder.close();
	file.close();
}

void ModeNotes::setupBrowser()
{
	filescount = 0;
	selection = 0;
	dirpos = 0;


	if (!folder.open("/notes"))
	{
		if (!sd->mkdir("notes"))
			Serial.println("Failed to create notes folder.");
	}

	folder.close();

	if (!folder.open("/notes"))
	{
		Serial.println("Failed to open notes folder.");
		d_pagecount = 0;
	}
	else
	{
		while (file.openNext(&folder, O_RDONLY))
		{
			if (file.isSubDir() || file.isHidden())
			{
				file.close();
				continue;
			}

			char name[64];
			file.getName(name, 64);
			String s = name;

			if (s.substring(s.lastIndexOf('.')) != ".note")
			{
				file.close();
				continue;
			}

			filescount++;

			file.close();
		}

		d_pagecount = ceil(filescount / 6.0);
	}

	folder.close();

	d_page = (d_pagecount > 0) ? 1 : 0;
}

MODE ModeNotes::react(byte id)
{
	if (mode == SM_BROWSER)
	{
		if (!file.isOpen())
		{
			if (id == 11 && selection > 0)
				selection--;

			else if (id == 12 && selection < filescount - 1)
				selection++;

			else if (id == 17)
			{
				if (this->openFile())
					this->display_file();

				return M_NOTES;
			}

			else if (id == 22)
			{
				if (folder.open("/notes") && file.open(&folder, dirpos, O_RDONLY))
				{
					char name[128];
					file.getName(name, 128);

					mode = SM_CONFIRM_DEL;
					this->display_confirm_del(String(name));

					file.close();
					folder.close();

					return M_NOTES;
				}

				file.close();
				folder.close();
			}

			else if (id == 23)
				return M_MENU;

			d_page = (selection / 6) + 1;

			this->display_browser();
		}
		else
		{
			if (id == 11 && f_page > 1)
				f_page--;

			else if (id == 12 && f_page < f_pagecount)
				f_page++;

			else if (id == 23)
			{
				file.close();
				folder.close();

				this->display_browser();

				return M_NOTES;
			}

			this->display_file();
		}
	}
	else if (mode == SM_CONFIRM_DEL)
	{
		if (id == 19)
		{
			deleteFile();
			setupBrowser();

			mode = SM_BROWSER;
			this->display_browser();
		}
		else if (id == 23)
		{
			mode = SM_BROWSER;
			this->display_browser();
		}
	}


	return M_NOTES;
}

bool ModeNotes::openFile()
{
	if (!folder.open("/notes") || !file.open(&folder, dirpos, O_RDONLY))
	{
		Serial.println("Could not open selected file!");
		folder.close();
		file.close();
		return false;
	}

	f_pagecount = (file.fileSize() / sizeof(uint16_t)) / CHARS_PER_PAGE;
	f_page = 1;

	return true;
}

bool ModeNotes::deleteFile()
{
	if (!folder.open("/notes") || !file.open(&folder, dirpos, O_WRITE))
	{
		Serial.println("Could not open selected file!");
		folder.close();
		file.close();
		return false;
	}

	return file.remove();
}

void ModeNotes::display_browser()
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->setTextSize(1);
	dsp->print("Browsing notes (");
	dsp->print(filescount);
	dsp->println(")");
	dsp->print("Page (");
	dsp->print(d_page);
	dsp->print("/");
	dsp->print(d_pagecount);
	dsp->println(")");

	if (filescount == 0)
	{
		dsp->println("No files found.");
		dsp->display();
		return;
	}

	unsigned int i = 0;

	if (!folder.open("/notes"))
	{
		Serial.println("Failed to open notes folder.");
		folder.close();
		return;
	}

	while (file.openNext(&folder, O_RDONLY))
	{
		if (file.isSubDir() || file.isHidden())
		{
			file.close();
			continue;
		}
		
		if (i >= 6 * d_page)
			break;

		if (i >= 6 * (d_page - 1))
		{
			char name[18];
			file.getName(name, 18);
			
			if (selection == i)
			{
				dirpos = file.dirIndex();
				memcpy(filename, name, sizeof(char) * 18);

				dsp->print(">");
				dsp->println(name);
			}
			else
				dsp->println(name);
		}

		file.close();

		i++;
	}
	
	folder.close();
	file.close();

	dsp->display();
}

void ModeNotes::display_file()
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->setTextSize(1);
	dsp->println(filename);
	dsp->print("Page (");
	dsp->print(f_page);
	dsp->print("/");
	dsp->print(f_pagecount);
	dsp->println(")");

	file.seekSet((f_page - 1) * CHARS_PER_PAGE * sizeof(uint16_t));

	uint16_t nextchar;

	for(int i = 0; i < CHARS_PER_PAGE; i++)
	{
		if (file.available() == 0)
			break;
		
		file.read(&nextchar, sizeof(uint16_t));

		if (nextchar <= 255 && nextchar != 172 && nextchar != 176 && nextchar != 177)
			dsp->print((char) nextchar);
		else
			drawChar(nextchar, dsp);
	}

	dsp->display();
}

void ModeNotes::display_confirm_del(String filename)
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->println("Confirm deletion of:");
	dsp->println(filename);

	dsp->println("Press M+ to delete.");
	dsp->println("Press C to return.");

	dsp->display();
}