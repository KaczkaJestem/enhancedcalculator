#pragma once
#include "Mode.h"
#include "Vector.h"

class ModeSnake : public Mode
{
private:
	struct Point
	{
		byte x;
		byte y;
	};

	enum DIRECTION
	{
		UP,
		DOWN,
		LEFT,
		RIGHT
	};

	byte keyMap[24];
	
	bool pause;
	bool start;
	bool lost;
	
	int score;
	double speed;
	DIRECTION dir, lastmove;
	Point apple;

	Point storage[255];
	Vector<Point> snake;

	unsigned int time;

public:
	ModeSnake(Adafruit_SSD1306* display, SdFat* SD);
	~ModeSnake();

	virtual MODE react(byte id);
	virtual void update();

private:
	bool move();
	void grow(byte x, byte y);
	void newapple();

	void loose();

	void display_rules();
	void display_pause();
	void display();
};
