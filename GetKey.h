#pragma once
#include <Keypad.h>

#define UNCERTAINTY 50

byte GetKey(Keypad& keypad, byte& lastkey)
{
	byte id = keypad.getKey();

	if (id != NO_KEY)
		return id;

	if (analogRead(A0) != 0)
		if (abs(analogRead(A0) - 90) < UNCERTAINTY)
			id = 17;
		else if (analogRead(A0) > 500)
			id = 18;

	if (analogRead(A1) != 0)
		if (abs(analogRead(A1) - 90) < UNCERTAINTY)
			id = 19;
		else if (analogRead(A1) > 500)
			id = 20;

	if (analogRead(A2) != 0)
		if (abs(analogRead(A2) - 90) < UNCERTAINTY)
			id = 21;
		else if (analogRead(A2) > 500)
			id = 22;

	if (analogRead(A3) != 0)
		if (abs(analogRead(A3) - 90) < UNCERTAINTY)
			id = 23;
		else if (analogRead(A3) > 500)
			id = 24;

	if (id == lastkey)
		return NO_KEY;
	else
	{
		lastkey = id;
		return id;
	}
}