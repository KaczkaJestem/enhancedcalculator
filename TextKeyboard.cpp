#include "TextKeyboard.h"

TextKeyboard::TextKeyboard()
{
	ABC = true;
	text = "";
	limit = 255;

	time = 0;

	caret = 0;
	last_id = 255;
	key_iter = 0;
	max_iter = false;

	sealed = false;
}

TextKeyboard::~TextKeyboard()
{

}

void TextKeyboard::react(byte key)
{
	unsigned int now = millis();
	bool newpress = false;

	// New button pressed?
	if (now - time > 1000)
	{
		newpress = true;

		if (text.length() == limit)
			sealed = true;

		time = now;
	}

	if ((key >= 0 && key <= 9) || key == 15 || key == 16)
	{
		// IF literal mode is ON:
		if (ABC)
		{
			char c;

			// Manage char appending/change:
			if (key == last_id && !newpress)
			{
				if (max_iter)
				{
					key_iter = 0;
					max_iter = false;
				}
				else
					key_iter++;
			}
			else
			{
				max_iter = false;
				key_iter = 0;
			}

			// Keyboard layout:
			switch (key)
			{
			case 0:
				if (key_iter == 0)		c = ' ';
				else if (key_iter == 1)	c = '!';
				else if (key_iter == 2)	c = '?';
				if (key_iter == 2)		max_iter = true;
				break;
			case 1:
				if (key_iter == 0)		c = 's';
				else if (key_iter == 1)	c = 't';
				else if (key_iter == 2)	c = 'u';
				if (key_iter == 2)		max_iter = true;
				break;
			case 2:
				if (key_iter == 0)		c = 'v';
				else if (key_iter == 1)	c = 'w';
				else if (key_iter == 2)	c = 'x';
				if (key_iter == 2)		max_iter = true;
				break;
			case 3:
				if (key_iter == 0)		c = 'y';
				else if (key_iter == 1)	c = 'z';
				if (key_iter == 1)		max_iter = true;
				break;
			case 4:
				if (key_iter == 0)		c = 'j';
				else if (key_iter == 1)	c = 'k';
				else if (key_iter == 2)	c = 'l';
				if (key_iter == 2)		max_iter = true;
				break;
			case 5:
				if (key_iter == 0)		c = 'm';
				else if (key_iter == 1)	c = 'n';
				else if (key_iter == 2)	c = 'o';
				if (key_iter == 2)		max_iter = true;
				break;
			case 6:
				if (key_iter == 0)		c = 'p';
				else if (key_iter == 1)	c = 'q';
				else if (key_iter == 2)	c = 'r';
				if (key_iter == 2)		max_iter = true;
				break;
			case 7:
				if (key_iter == 0)		c = 'a';
				else if (key_iter == 1)	c = 'b';
				else if (key_iter == 2)	c = 'c';
				if (key_iter == 2)		max_iter = true;
				break;
			case 8:
				if (key_iter == 0)		c = 'd';
				else if (key_iter == 1)	c = 'e';
				else if (key_iter == 2)	c = 'f';
				if (key_iter == 2)		max_iter = true;
				break;
			case 9:
				if (key_iter == 0)		c = 'g';
				else if (key_iter == 1)	c = 'h';
				else if (key_iter == 2)	c = 'i';
				if (key_iter == 2)		max_iter = true;
				break;

			case 15:
				if (key_iter == 0)		c = '(';
				else if (key_iter == 1)	c = ')';
				if (key_iter == 1)		max_iter = true;
				break;
			case 16:
				if (key_iter == 0)		c = '.';
				else if (key_iter == 1)	c = ',';
				else if (key_iter == 2)	c = '^';
				if (key_iter == 2)		max_iter = true;
				break;
			default:
				break;
			}

			if (key == last_id && !newpress)
			{
				if (!sealed)
					changeLast(c);

				time = now;
			}
			else
			{
				append(c);
				time = now;
			}
		}
		// IF ABC == false then append numbers:
		else
		{	
			if (key == 16)
				append('.');
			else
				append(key + 48);
		}
	}

	// Regardless if ABC is on or not:
	if (key == 10)
		append('%');
	else if (key == 11)
		append('-');
	else if (key == 12)
		append('+');
	else if (key == 13)
		append('/');
	else if (key == 14)
		append('*');
	else if (key == 18)
		append("sqrt(");
	else if (key == 19)
		ABC = !ABC;
	else if (key == 20 && caret > 0)
		caret--;
	else if (key == 21 && caret < text.length())
		caret++;
	else if (key == 22)
		erase();
	
	last_id = key;
}

void TextKeyboard::setText(String txt, byte charlimit)
{
	limit = charlimit;
	text = txt;
	caret = txt.length();
}

void TextKeyboard::clearText()
{
	text = "";
	caret = 0;
}

bool TextKeyboard::isLiteralModeOn()
{
	return ABC;
}

String TextKeyboard::getText()
{
	return text;
}

byte TextKeyboard::getLimit()
{
	return limit;
}

byte TextKeyboard::getCaretPos()
{
	return caret;
}

void TextKeyboard::append(char c)
{
	if (text.length() < limit)
	{
		text = text.substring(0, caret) + c + text.substring(caret);
		caret++;
	}
}

void TextKeyboard::append(String str)
{
	if (text.length() + str.length() < limit)
	{
		text = text.substring(0, caret) + str + text.substring(caret);
		caret += str.length();
	}
}

void TextKeyboard::changeLast(char c)
{
	text = text.substring(0, caret - 1) + c + text.substring(caret);
}

void TextKeyboard::erase()
{
	if (text.length() == 0)
		return;

	text = text.substring(0, caret - 1) + text.substring(caret);
	caret--;
	sealed = false;
}