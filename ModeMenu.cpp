#include "ModeMenu.h"

ModeMenu::ModeMenu(Adafruit_SSD1306* display, SdFat* SD)
	: Mode(display, SD)
{
	Serial.println("MODE: Menu");

	mode[0] = "1. Calculator";
	mode[1] = "2. Notes";
	mode[2] = "3. Plots";
	mode[3] = "4. Let me play!";
	mode[4] = "5. Chatroom";
	mode[5] = "6. Change keycode";

	selection = 0;

	this->display();
}

ModeMenu::~ModeMenu()
{

}

MODE ModeMenu::react(byte id)
{
	if (id >= 1 && id <= _modecount)
		selection = id - 1;

	else if (id == 12 && selection < _modecount - 1)
		selection++;
	else if (id == 11 && selection > 0)
		selection--;
	else if (id == 17)
	{
		if (selection == 0)
			return M_STANDARD;
		else if (selection == 1)
			return M_NOTES;
		else if (selection == 2)
			return M_PLOTS;
		else if (selection == 3)
			return M_SNAKE;
		else if (selection == 4)
			return M_CHAT;
		else if (selection == 5)
			return M_CODECHANGE;
	}

	this->display();
	return M_MENU;
}

void ModeMenu::display()
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->setTextSize(2);
	dsp->println("Mode Menu");

	dsp->setTextSize(1);
	
	for (int i = 0; i < _modecount; i++)
	{
		if (selection == i)
			dsp->println(">" + mode[i] + "<");
		else
			dsp->println(mode[i]);
	}

	dsp->display();
}