#pragma once
#include "Mode.h"

class ModeMenu : public Mode
{
private:
	byte keyMap[24];

	static const byte _modecount = 6;
	String mode[_modecount];

	byte selection;

public:
	ModeMenu(Adafruit_SSD1306* display, SdFat* SD);
	~ModeMenu();

	virtual MODE react(byte id);

	void display();
};
