#include "ModeChat.h"

ModeChat::Session ModeChat::session;

ModeChat::ModeChat(Adafruit_SSD1306* display, SdFat* SD)
	: Mode(display, SD)
{
	Serial.println(F("MODE: Chat"));

	browsing_index = -1;
	servers_count = 0;
	browser_page = 0;

	time_blinker = 0;
	blinker = false;

	chatline_offset = 0;
	saveindex = -1;


	if (!session.available)
	{
		this->displayWaitScreen(F("Initializing..."));

		reset(true);
		Settings settings;

		if (!readSettings(sd, settings))
		{
			mode = SM_ERROR;

			String message = F("Cannot read settings!\n");
			message += F("Please check SD card.");

			this->displayError(message);
		}
		else
		{
			txt_kbd.setText(settings.username, 10);
			mode = SM_NAMESETUP;

			this->displaySetup();
		}
	}
	else
	{
		mode = SM_CHAT;
		this->displayChat();
	}
}

ModeChat::~ModeChat()
{
}

MODE ModeChat::react(byte id)
{
	if (mode == SM_ERROR)
	{
		exit();
		return M_MENU;
	}
	else if (mode == SM_NAMESETUP)
	{
		if (id == 17 || id == 18)
		{
			// EQUALS - SEARCH FOR SERVERS
			// OR
			// SQUARE ROOT - START SERVER

			if (txt_kbd.getText().length() > 0)
			{
				Settings settings;
				readSettings(sd, settings);

				session.name = txt_kbd.getText();
				
				memcpy(settings.username, session.name.c_str(), sizeof(char) * session.name.length() + 1);
				saveSettings(sd, settings);

				txt_kbd.setText("", 40);
				

				if (id == 17)
				{
					this->displayWaitScreen(F("Scanning..."));

					mode = SM_BROWSER;
					getAPs();

					this->displayBrowser();
				}
				if (id == 18)
				{
					this->displayWaitScreen(F("Setting up server..."));

					if (setupServer())
					{
						mode = SM_CHAT;
						clearChatLog(sd);

						this->displayChat();
					}
					else
					{
						mode = SM_ERROR;
						this->displayError(F("Failed to start server!"));
					}
				}

				return M_CHAT;
			}
		}
		else if (id == 23)
		{
			return M_MENU;
		}
		else
			txt_kbd.react(id);

		this->displaySetup();
	}
	else if (mode == SM_BROWSER)
	{
		// QUIT
		if (id == 23)
		{
			return M_MENU;
		}
		// REFRESH SCAN
		else if (id == 19)
		{
			this->displayWaitScreen(F("Scanning..."));
			getAPs();

			this->displayBrowser();
		}
		// SELECT PREVIOUS
		else if (id == 11)
		{
			browsing_index = (browsing_index > 0) ? browsing_index - 1 : browsing_index;

			if (browsing_index < (browser_page) * 6)
				browser_page--;
		}
		// SELECT NEXT
		else if (id == 12)
		{
			browsing_index = (browsing_index < servers_count - 1)? browsing_index + 1 : browsing_index;

			if (browsing_index >= (browser_page + 1) * 6)
				browser_page++;
		}
		// SELECT
		else if (id == 17 && browsing_index != -1)
		{
			this->displayWaitScreen(F("Connecting..."));

			String ap_name;
			int begin = 0, iter = 0;

			while (begin < server_list.length())
			{
				int cut = (server_list.indexOf(' ', begin) != -1) ? server_list.indexOf(' ', begin) : server_list.length();

				String name = server_list.substring(0, cut);
				begin = cut + 1;

				if (browsing_index == iter)
				{
					ap_name = name;
					break;
				}

				iter++;
			}

			if (ap_name.length() > 0 && setupClient(ap_name))
			{
				mode = SM_CHAT;
				clearChatLog(sd);

				this->displayChat();
				return M_CHAT;
			}
			else
			{	
				mode = SM_ERROR;
				this->displayError(F("Cannot connect to the server!"));

				return M_CHAT;
			}
		}

		this->displayBrowser();
	}
	else if (mode == SM_CHAT)
	{
		// SEND MESSAGE
		if (id == 17 && txt_kbd.getText().length() > 0)
		{
			String msg = txt_kbd.getText();
			msg.replace(' ', '_');

			sendMessage(msg);

			txt_kbd.clearText();
			this->displayChat();
		}
		// MINIMIZE QUIT
		else if (id == 23)
		{
			return M_MENU;
		}


		if (txt_kbd.isLiteralModeOn() && id == 12)
		{
			// PLUS ON LITERAL - SCROLL DOWN
			if(chatline_offset > 0)
				chatline_offset--;
		}
		else if (txt_kbd.isLiteralModeOn() && id == 11)
		{
			// MINUS ON LITERAL - SCROLL UP
			if(chatline_offset + 5< session.lines_count)
				chatline_offset++;
		}
		else if (txt_kbd.isLiteralModeOn() && id == 18)
		{
			// SQUARE ROOT ON LITERAL - GO TO THE TOP
			chatline_offset = 0;
		}
		else if (!txt_kbd.isLiteralModeOn() && id == 15)
		{
			// PLUS/MINUS ON NUMERIC - SAVE CHAT TO NOTES
			displayWaitScreen(F("Saving chat to notes..."));

			if (!saveChatLogToNotes(sd, saveindex))
				Serial.println(F("Saving chat log failed!"));
		}
		else if (txt_kbd.isLiteralModeOn() && id == 10)
		{
			// PERCENT ON LITERAL - DEFINITIVE EXIT
			this->exit();
			return M_MENU;
		}
		else
			txt_kbd.react(id);

		this->displayChat();
	}

	return M_CHAT;
}

void ModeChat::update()
{
	unsigned int now = millis();

	if ( (mode == SM_NAMESETUP || mode == SM_CHAT) && now - time_blinker > 500)
	{
		blinker = !blinker;
		
		if (mode == SM_NAMESETUP)
			this->displaySetup();
		else if (mode == SM_CHAT)
			this->displayChat();

		time_blinker = now;
	}
}

bool ModeChat::setupServer()
{
	String cmd = F("ssap ");
	cmd += F("@");
	cmd += session.name;

	Serial1.println(cmd);
	cmd = readESP();

	if (cmd.indexOf(F("ss_ok")) < 0)
	{
		Serial.println(cmd);
		return false;
	}

	session.available = true;
	session.client_count = 0;
	session.lines_count = 0;

	return true;
}

bool ModeChat::setupClient(String ap_name)
{
	String cmd = F("scon ");
	cmd += ap_name;

	Serial1.println(cmd);

	cmd = readESP();

	if (cmd.indexOf(F("con_ok")) < 0)
	{
		Serial.println(cmd);
		return false;
	}

	Serial1.println(F("get_cc"));
	cmd = readESP();

	if (!cmd.startsWith(F("cmd_cc")))
	{
		Serial.println(cmd);
		return false;
	}

	session.available = true;
	session.client_count = cmd.substring(cmd.indexOf(' ') + 1).toInt();
	session.lines_count = 0;

	return true;
}

void ModeChat::sendMessage(String message)
{
	String cmd = "msg ";
	cmd += session.name;
	cmd += ':';
	cmd += message;

	Serial1.println(cmd);
}

void ModeChat::getAPs()
{
	Serial1.println(F("ap_lst"));

	String response = readESP();

	if (response.indexOf(F("err_to")) >= 0 || response.indexOf(F("err_lst")) >= 0)
	{
		Serial.println(response);
		return;
	}

	response.trim();

	int begin = 0;
	servers_count = 0;
	
	while (begin < response.length())
	{
		int cut = (response.indexOf(' ', begin) != -1) ? response.indexOf(' ', begin) : response.length();
		
		if (response.substring(begin, cut).startsWith(F("@")))
		{
			server_list += response.substring(begin, cut);
			servers_count++;
		}

		begin = cut + 1;
	}

	if (servers_count > 0)
	{
		browser_page = 0;
		browsing_index = 0;
	}
}

void ModeChat::exit()
{
	reset(false);
	session.available = false;
}

bool ModeChat::reset(bool quiet)
{
	if (!quiet)
	{
		dsp->clearDisplay();
		dsp->setCursor(0, 0);

		dsp->println("Resetting...");
		dsp->display();
	}

	Serial1.println(F("rst"));

	if (readESP().indexOf(F("rst_ok")) < 0)
	{
		Serial.println(F("ESP reset failed!"));
		return false;
	}

	return true;
}

void ModeChat::displayError(String err)
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);
	dsp->print(err);

	dsp->display();
}

void ModeChat::displaySetup()
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);
	
	if (txt_kbd.isLiteralModeOn())
		dsp->println(F("[ABC]"));
	else
		dsp->println(F("[123]"));

	dsp->println(F("Your username (10):"));

	dsp->println(txt_kbd.getText());

	dsp->println();
	dsp->println();
	dsp->println();
	dsp->println(F("[=] search for rooms"));
	dsp->println(F("[sqrt] host room"));

	if (blinker)
	{
		byte caret = txt_kbd.getCaretPos();
		dsp->drawLine(0 + (caret % 21) * 6, 16 + (caret / 21) * 8, 0 + (caret % 21) * 6, 24 + (caret / 21) * 8, WHITE);
	}

	dsp->display();
}

void ModeChat::displayBrowser()
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->println(F("Press [M+] to rescan"));
	dsp->print(F("Servers found ("));
	dsp->print(servers_count);
	dsp->println(F("):"));

	
	int iter = 0;
	int begin = 0;

	while(begin < server_list.length())
	{
		int cut = (server_list.indexOf(' ', begin) != -1)? server_list.indexOf(' ', begin) : server_list.length();
		
		String name = server_list.substring(begin, cut);
		begin = cut + 1;

		if (browsing_index == iter + 6 * browser_page)
		{
			dsp->print(F(">"));
			dsp->println(name);
		}
		else
			dsp->println(name);

		iter++;
	}

	dsp->display();
}

void ModeChat::displayChat()
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->print("> ");
	dsp->println(txt_kbd.getText());

	if (txt_kbd.getText().length() < 20)
		dsp->println();

	String page;

	if (!readChatLog(sd, page, 21 * 5, chatline_offset))
	{
		dsp->println(F("Cannot find log file!"));
		dsp->println(F("Error. Chat will be  "));
		dsp->println(F("   not available!    "));
		dsp->println();
		dsp->println();
	}
	else if (!session.available)
	{
		dsp->println(F("Session disconnected!"));
		dsp->println();
		dsp->println();
		dsp->println();
		dsp->println();
	}
	else
	{
		dsp->println(page);
	}

	dsp->drawFastHLine(0, 15, 128, WHITE);
	dsp->drawFastHLine(0, 55, 128, WHITE);


	dsp->print(F("Users: "));

	String u_count(session.client_count);

	for (int i = u_count.length(); i < 4; i++)
		u_count += " ";

	dsp->print(u_count);
	
	if(chatline_offset != 0)
		dsp->print(F(" -v- "));
	else
		dsp->print(F("     "));

	if (txt_kbd.isLiteralModeOn())
		dsp->println(F("[ABC]"));
	else
		dsp->println(F("[123]"));

	if (blinker)
	{
		byte caret = txt_kbd.getCaretPos();

		if(caret != txt_kbd.getLimit())
			dsp->drawLine(((2 + caret) % 21) * 6, 0 + ((2 + caret) / 21) * 8, ((2 + caret) % 21) * 6, 7 + ((2 + caret) / 21) * 8, WHITE);
	}

	dsp->display();
}

void ModeChat::displayWaitScreen(String info)
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->println(F("Please be patient..."));
	dsp->println();
	dsp->print(info);

	dsp->display();
}