#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>

#define MAX_SRV_CLIENTS 8

enum MODE {
  M_NOT_INITIALIZED,
  M_SERVER,
  M_CLIENT
} mode;

const char *password = "011235813";
uint8_t client_cnt = 0;
unsigned long client_timeout = 0;
int indicator = 0;
uint8_t ping_timeout = 0;

typedef struct Message {
  String msg = "";
  int id = -1;
} Message;

Message chatlog[10];

ESP8266WebServer* server = NULL;

void reset() {
  WiFi.disconnect();
  
  if(server != NULL)
  {
    server->stop();
    WiFi.softAPdisconnect(true);
  }

  delete server;
  server = NULL;

  client_cnt = 0;
  client_timeout = 0;
  indicator = 0;
  ping_timeout = 0;
  
  mode = M_NOT_INITIALIZED;
}

void note(String msg) {
  indicator++;

  if(indicator < 0) {
    indicator = 0;
  }

  for(int i = 8; i >= 0; i--)
  {
    chatlog[i+1] = chatlog[i];
  }

   chatlog[0].msg = msg;
   chatlog[0].id = indicator;
}

String request(String req, int* code = NULL) {
  if (WiFi.status() == WL_CONNECTED){
    HTTPClient http;
    String link = "http://192.168.1.1";
    link += req;
    
    http.begin(link);
    
    int httpCode = http.GET();
    unsigned long timeout = millis();
    
    while (httpCode < 0) {
      ESP.wdtFeed();
      httpCode = http.GET();

      if(millis() - timeout > 5000)
      {
        http.end();
        return "err_to";
      }
    }

    String out = http.getString();

    if(code != NULL)
    *code = httpCode;
    
    http.end();   //Close connection
    
    return out;
 }
}

bool setupClient(String name) {
  reset();

  WiFi.mode(WIFI_STA);
  WiFi.begin(name.c_str(), password);

  // Timeout set to 30 seconds
  int timeout = 60;
  
  while (WiFi.status() != WL_CONNECTED) {
    ESP.wdtFeed();
    delay(500);

    if(timeout == 0)
      return false;
    
    timeout--;
  }

  request("/flow?get=ind", &indicator);
  
  mode = M_CLIENT;
  return true;
}

bool setupServer(String name) {
  reset();

  IPAddress local_ip(192,168,1,1);
  IPAddress gateway(192,168,1,1);
  IPAddress subnet(255,255,255,0);
  
  server = new ESP8266WebServer(80);

  WiFi.softAP(name.c_str(), password);
  WiFi.softAPConfig(local_ip, gateway, subnet);
  
  server->on("/msg", HTTP_GET, handleMessage);
  server->on("/flow", HTTP_GET, handleUpdate);
  server->begin();

  mode = M_SERVER;
  return true;
}

void loopServer() {
  server->handleClient();
  
  if(client_cnt != WiFi.softAPgetStationNum())
  {
    client_cnt = WiFi.softAPgetStationNum();

    String cmd = "cmd_cc ";
    cmd += client_cnt;
    Serial.println(cmd);
    note(cmd);
  }
}

void loopClient() {
  // Check for incoming messages
  if(millis() - client_timeout > 500) {
    if (WiFi.status() == WL_CONNECTED)
     {
      int ind = indicator;
      String req = "/flow?ind=";
      req += indicator;
      
      String out = request(req, &ind);
  
      if(out.equals("ok") || out.equals("empty")) {
        ping_timeout = 0;
      }
      else if(out.equals("err_to")) {
        ping_timeout++;
      }
      else {
        ping_timeout = 0;
        indicator = ind;
        Serial.println(out);
      }
    }
    else {
      ping_timeout++;
    }

    client_timeout = millis();
  }

  if(ping_timeout >= 2)
  {
    Serial.println("err_disc");
    reset();
  }
}

void handleMessage() {
  if (server->hasArg("message") && server->hasArg("nick")) {

    String nick = server->arg("nick");
    String msg = server->arg("message");
    
    String output = "msg ";
    output += nick;
    output += ':';
    output += msg;
    
    server->send(200, "text/html", "ok");

    Serial.println(output);
    note(output);
  }
}

void handleUpdate() {
  if(server->hasArg("ind")) {
    unsigned int client_ind = server->arg("ind").toInt();
    
    if(client_ind == indicator)
      server->send(indicator, "text/html", "ok");
    else {
      Message* msg = NULL;

      for(int i = 0; i < 10; i++)
      {
        if(chatlog[i].id == -1)
          break;
 
        msg = &chatlog[i];
        if(chatlog[i].id == client_ind + 1)
          break;
      }

      if(msg != NULL)
        server->send(msg->id, "text/html", msg->msg);
      else
        server->send(0, "text/html", "empty");
    }
  }
  else if(server->hasArg("get"))
  {
    if(server->arg("get").equals("ind"))
      server->send(indicator, "text/html", "ok");
    else if(server->arg("get").equals("cc"))
      server->send(client_cnt, "text/html", "ok");
  }
}

void setup() {
    mode = M_NOT_INITIALIZED;
    Serial.begin(115200);
}

void listenOnUART(){
    if(Serial.available() > 0){
      String input = Serial.readString();
      input.trim();

      if(input.length() > 0 && input.charAt(input.length() - 1) == '\n')
        input = input.substring(0, input.length() - 1);
  
      int split = input.indexOf(' ');
      String cmd = input.substring(0, split);
  
      if(split + 1 < input.length())
        input = input.substring(split + 1);
      else
        input = "";
      
      if(cmd.equals("ssap") && input.length() > 0) {
          if(setupServer(input))
            Serial.println("ss_ok");
          else
            Serial.println("err_sfs");
      }
      else if(cmd.equals("scon") && input.length() > 0) {
          if(setupClient(input))
            Serial.println("con_ok");
          else
            Serial.println("err_cf"); 
      }
      else if(cmd.equals("msg") && input.length() > 0 && mode != M_NOT_INITIALIZED) {
          String msg = "msg ";
          msg += input;
          
          if(mode == M_CLIENT) {

            int semi = input.indexOf(':');
            String nick = input.substring(0, semi);

            if(semi + 1 < input.length())
              input = input.substring(semi + 1);
            else
              input = "";

            String req = "/msg?nick=";
            req += nick;
            req += "&message=";
            req += input;
            
            request(req);
          }
          else {
            note(msg);
            Serial.println(msg);
          }
    }
    else if(cmd.equals("rst")) {
      reset();
      Serial.println("rst_ok");
    }
    else if(cmd.equals("get_cc")) {
      String output = "cmd_cc ";

      if(mode == M_SERVER)
        output += client_cnt;
      else if(mode == M_CLIENT)
      {
        int count;
        request("/flow?get=cc", &count);
        output += count;
      }
      else 
        output += 0;
      
      Serial.println(output);
    }
    else if(cmd.equals("get_in")) {
      String output = "cmd_in ";
      output += indicator;
      
      Serial.println(output);
    }
    else if(cmd.equals("fch_in")) {
      if(mode == M_CLIENT)
      {
        String output = "cmd_in ";

        int ind;
        request("/flow?get=ind", &ind);
        output += ind;

        indicator = ind;
        Serial.println(output);
      }
      else {
        String output = "cmd_in ";
        output += indicator;
      
        Serial.println(output);
      }
    }
    else if(cmd.equals("ap_lst"))
    {
      if(mode == M_NOT_INITIALIZED)
      {
        WiFi.disconnect();
        int n = WiFi.scanNetworks();
        String aps = "";
        
        for (int i = 0; i < n; i++) {
          if(aps.length() < 255)
            aps += WiFi.SSID(i);
            aps += " ";
        }
  
        Serial.println(aps);
      }
      else 
        Serial.println("err_lst");
    }
    else if(cmd.length() > 0)
    {
      Serial.println("cmd_nr");
    }
  }
}

void loop() {
    listenOnUART();

    if(mode == M_SERVER)
      loopServer();
     else if(mode == M_CLIENT)
      loopClient();
   
}
