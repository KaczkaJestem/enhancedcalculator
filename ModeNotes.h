#pragma once
#include "Mode.h"
#include "FileManagement.h"

class ModeNotes : public Mode
{
private:
	enum SUBMODE
	{
		SM_BROWSER,
		SM_CONFIRM_DEL
	};

	byte keyMap[24];
	SdFile folder;
	SdFile file;

	SUBMODE mode;

	uint32_t dirpos;
	char filename[18];

	static const unsigned int CHARS_PER_PAGE = 21 * 6;

	unsigned int f_page;
	unsigned int f_pagecount;

	unsigned int selection;

	unsigned int d_page;
	unsigned int d_pagecount;

	unsigned int filescount;

	void setupBrowser();

public:
	ModeNotes(Adafruit_SSD1306* display, SdFat* SD);
	~ModeNotes();

	virtual MODE react(byte id);

	bool openFile();
	bool deleteFile();

	void display_browser();
	void display_file();
	void display_confirm_del(String filename);
};