#pragma once

void drawChar(uint16_t charcode, Adafruit_SSD1306* display)
{
	int16_t x = display->getCursorX();
	int16_t y = display->getCursorY();

	if (x + 6 > 127)
	{
		x = 0;
		y += 8;
	}

	if (charcode == 8747)
	{
		// INTEGRAL
		display->drawPixel(x, y + 6, WHITE);
		display->drawPixel(x + 1, y + 7, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 3, y + 1, WHITE);
		display->drawPixel(x + 4, y, WHITE);
		display->drawPixel(x + 5, y + 1, WHITE);
	}
	else if (charcode == 8730)
	{
		// SQARE ROOT
		display->drawPixel(x, y + 4, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 3, y + 7, WHITE);
		display->drawPixel(x + 3, y + 6, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 3, y + 1, WHITE);
		display->drawPixel(x + 3, y, WHITE);
		display->drawPixel(x + 4, y, WHITE);
		display->drawPixel(x + 5, y, WHITE);
	}
	else if (charcode == 177)
	{
		// PLUS/MINUS
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 3, y + 6, WHITE);
	}
	else if (charcode == 8734)
	{
		// INFINITY
		display->drawPixel(x, y + 4, WHITE);
		display->drawPixel(x, y + 3, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
	}
	else if (charcode == 172)
	{
		// NEGATION
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
	}
	else if (charcode == 916)
	{
		// DELTA
		display->drawPixel(x, y + 4, WHITE);
		display->drawPixel(x, y + 5, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
	}
	else if (charcode == 8719)
	{
		// CAPITAL PI
		display->drawPixel(x, y, WHITE);
		display->drawPixel(x + 1, y, WHITE);
		display->drawPixel(x + 1, y + 1, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y, WHITE);
		display->drawPixel(x + 3, y, WHITE);
		display->drawPixel(x + 4, y, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
		display->drawPixel(x + 5, y, WHITE);
	}
	else if (charcode == 960)
	{
		// PI
		display->drawPixel(x, y + 2, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
	}
	else if (charcode == 8721)
	{
		// CAPITAL SIGMA
		display->drawPixel(x, y, WHITE);
		display->drawPixel(x + 1, y, WHITE);
		display->drawPixel(x + 2, y, WHITE);
		display->drawPixel(x + 3, y, WHITE);
		display->drawPixel(x + 4, y, WHITE);
		display->drawPixel(x + 5, y, WHITE);
		display->drawPixel(x, y + 6, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 3, y + 6, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
		display->drawPixel(x + 5, y + 6, WHITE);

		display->drawPixel(x, y + 1, WHITE);
		display->drawPixel(x, y + 5, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
	}
	else if (charcode == 8776)
	{
		// APPROX
		display->drawPixel(x, y + 3, WHITE);
		display->drawPixel(x, y + 6, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 3, y + 6, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
		display->drawPixel(x + 5, y + 2, WHITE);
		display->drawPixel(x + 5, y + 5, WHITE);
	}
	else if (charcode == 8800)
	{
		// NOT EQUAL
		display->drawPixel(x, y + 2, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 5, y + 2, WHITE);
		display->drawPixel(x, y + 5, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 5, y + 5, WHITE);
		display->drawPixel(x, y + 7, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
		display->drawPixel(x + 5, y, WHITE);
	}
	else if (charcode == 8801)
	{
		// CONGRUENT
		display->drawPixel(x, y + 1, WHITE);
		display->drawPixel(x + 1, y + 1, WHITE);
		display->drawPixel(x + 2, y + 1, WHITE);
		display->drawPixel(x + 3, y + 1, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
		display->drawPixel(x + 5, y + 1, WHITE);
		display->drawPixel(x, y + 3, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 5, y + 3, WHITE);
		display->drawPixel(x, y + 5, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 5, y + 5, WHITE);
	}
	else if (charcode == 8804)
	{
		// LESS THAN/EQUAL
		display->drawPixel(x, y + 5, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 2, y + 7, WHITE);
		display->drawPixel(x + 3, y + 1, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
	}
	else if (charcode == 8805)
	{
		// GREATER THAN/EQUAL
		display->drawPixel(x + 1, y, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y + 1, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
		display->drawPixel(x + 3, y + 7, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
		display->drawPixel(x + 5, y + 5, WHITE);
	}
	else if (charcode == 8729)
	{
		// DOT
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
	}
	else if (charcode == 8723)
	{
		// MINUSPLUS
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
	}
	else if (charcode == 8834)
	{
		// PROPER INCLUSION LEFT
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
	}
	else if (charcode == 8835)
	{
		// PROPER INCLUSION RIGHT
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
	}
	else if (charcode == 8838)
	{
		// INCLUSION LEFT
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 7, WHITE);
		display->drawPixel(x + 2, y + 7, WHITE);
		display->drawPixel(x + 3, y + 7, WHITE);
		display->drawPixel(x + 4, y + 7, WHITE);
	}
	else if (charcode == 8839)
	{
		// INCLUSION RIGHT
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 1, y + 7, WHITE);
		display->drawPixel(x + 2, y + 7, WHITE);
		display->drawPixel(x + 3, y + 7, WHITE);
		display->drawPixel(x + 4, y + 7, WHITE);
	}
	else if (charcode == 8871)
	{
		// ENTAILS
		display->drawPixel(x + 1, y, WHITE);
		display->drawPixel(x + 1, y + 1, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
	}
	else if (charcode == 8866)
	{
		// INFERS
		display->drawPixel(x + 1, y, WHITE);
		display->drawPixel(x + 1, y + 1, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
	}
	else if (charcode == 8740)
	{
		// NOT DIVIDES
		display->drawPixel(x + 2, y, WHITE);
		display->drawPixel(x + 2, y + 1, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x, y + 5, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
	}
	else if (charcode == 8741)
	{
		// PARALLEL
		display->drawPixel(x + 1, y, WHITE);
		display->drawPixel(x + 1, y + 1, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 4, y, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
	}
	else if (charcode == 8742)
	{
		// NOT PARALLEL
		display->drawPixel(x + 1, y, WHITE);
		display->drawPixel(x + 1, y + 1, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 4, y, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
		display->drawPixel(x, y + 6, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 5, y + 1, WHITE);
	}
	else if (charcode == 8704)
	{
		// FOR ALL
		display->drawPixel(x, y, WHITE);
		display->drawPixel(x, y + 1, WHITE);
		display->drawPixel(x, y + 2, WHITE);
		display->drawPixel(x + 4, y, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 2, y + 7, WHITE);
	}
	else if (charcode == 8707)
	{
		// EXISTS
		display->drawPixel(x + 4, y, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
		display->drawPixel(x + 4, y + 7, WHITE);
		display->drawPixel(x, y, WHITE);
		display->drawPixel(x + 1, y, WHITE);
		display->drawPixel(x + 2, y, WHITE);
		display->drawPixel(x + 3, y, WHITE);
		display->drawPixel(x, y + 7, WHITE);
		display->drawPixel(x + 1, y + 7, WHITE);
		display->drawPixel(x + 2, y + 7, WHITE);
		display->drawPixel(x + 3, y + 7, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
	}
	else if (charcode == 8709)
	{
		// EMPTY SET
		display->drawPixel(x, y + 3, WHITE);
		display->drawPixel(x, y + 4, WHITE);
		display->drawPixel(x, y + 6, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 1, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 3, y + 1, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 3, y + 6, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 5, y + 1, WHITE);
		display->drawPixel(x + 5, y + 3, WHITE);
		display->drawPixel(x + 5, y + 4, WHITE);
	}
	else if (charcode == 8869)
	{
		// PERPENDICULAR
		display->drawPixel(x + 2, y + 1, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x, y + 6, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 3, y + 6, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
	}
	else if (charcode == 8745)
	{
		// INTERSECTION
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
	}
	else if (charcode == 8746)
	{
		// UNION
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
	}
	else if (charcode == 8501)
	{
		// ALEPH
		display->drawPixel(x, y + 1, WHITE);
		display->drawPixel(x, y + 6, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
		display->drawPixel(x + 5, y + 1, WHITE);
		display->drawPixel(x + 5, y + 2, WHITE);
		display->drawPixel(x + 5, y + 6, WHITE);
	}
	else if (charcode == 8502)
	{
		// BETH
		display->drawPixel(x, y + 1, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 3, y + 6, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
	}
	else if (charcode == 8711)
	{
		// NABLA
		display->drawPixel(x, y + 2, WHITE);
		display->drawPixel(x, y + 3, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
	}
	else if (charcode == 8712)
	{
		// CONTAINS
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 3, y + 6, WHITE);
	}
	else if (charcode == 8713)
	{
		// NOT CONTAINS
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 2, y + 6, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 3, y + 6, WHITE);
		display->drawPixel(x, y + 7, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
		display->drawPixel(x + 3, y + 3, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
	}
	else if (charcode == 8743)
	{
		// CONJUNCTION
		display->drawPixel(x, y + 5, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 2, y + 3, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
	}
	else if (charcode == 8744)
	{
		// DISJUNCTION
		display->drawPixel(x, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
	}
	else if (charcode == 176)
	{
		// DEGREE
		display->drawPixel(x, y + 1, WHITE);
		display->drawPixel(x + 1, y, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 2, y + 1, WHITE);
	}
	else if (charcode == 9669)
	{
		// IDEAL L
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 2, y + 2, WHITE);
		display->drawPixel(x + 2, y + 4, WHITE);
		display->drawPixel(x + 3, y + 1, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
		display->drawPixel(x + 4, y, WHITE);
		display->drawPixel(x + 4, y + 1, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 4, y + 6, WHITE);
	}
	else if (charcode == 9659)
	{
		// IDEAL R
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 3, y + 2, WHITE);
		display->drawPixel(x + 3, y + 4, WHITE);
		display->drawPixel(x + 2, y + 1, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 1, y, WHITE);
		display->drawPixel(x + 1, y + 1, WHITE);
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 1, y + 6, WHITE);
	}
	else if (charcode == 8852)
	{
		// DISJOINT UNION
		display->drawPixel(x + 1, y + 2, WHITE);
		display->drawPixel(x + 1, y + 3, WHITE);
		display->drawPixel(x + 1, y + 4, WHITE);
		display->drawPixel(x + 4, y + 2, WHITE);
		display->drawPixel(x + 4, y + 3, WHITE);
		display->drawPixel(x + 4, y + 4, WHITE);
		display->drawPixel(x + 1, y + 5, WHITE);
		display->drawPixel(x + 4, y + 5, WHITE);
		display->drawPixel(x + 2, y + 5, WHITE);
		display->drawPixel(x + 3, y + 5, WHITE);
	}
	else
	{
		// BLANK CHARACTER
		display->drawRect(x, y, 5, 8, WHITE);
	}

	if (x + 6 < 127)
		display->setCursor(x + 6, y);
	else
		display->setCursor(0, y + 8);
}