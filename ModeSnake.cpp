#include "ModeSnake.h"

ModeSnake::ModeSnake(Adafruit_SSD1306* display, SdFat* SD)
	: Mode(display, sd)
{
	Serial.println("MODE: Snake");

	pause = false;
	start = false;
	lost = false;

	score = 0;
	speed = 1.0;

	time = 0;

	// Start point in a 3 x 3 grid:
	byte x = random(2, 35);
	byte y = random(0, 14);

	snake.setStorage(storage);

	Point p;
	
	// Snake starting segments in pixels:
	p.x = 1 + 3 * (x - 2);
	p.y = 1 + 3 * y;
	snake.push_back(p);

	p.x = 1 + 3 * (x - 1);
	p.y = 1 + 3 * y;
	snake.push_back(p);

	p.x = 1 + 3 * x;
	p.y = 1 + 3 * y;
	snake.push_back(p);

	dir = RIGHT;
	lastmove = RIGHT;
	newapple();

	this->display_rules();
}

ModeSnake::~ModeSnake()
{
}

MODE ModeSnake::react(byte id)
{
	if (!start)
	{
		start = true;
	}
	else if (lost)
	{
		return M_MENU;
	}
	else if (pause)
	{
		if (id == 17)
			pause = false;
		else if (id == 23)
			return M_MENU;
	}
	else
	{
		if (id == 5)
			dir = DOWN;
		else if (id == 8)
			dir = UP;
		else if (id == 4)
			dir = LEFT;
		else if (id == 6)
			dir = RIGHT;
		else if (id == 23)
			pause = true;
	}

	this->display();
	return M_SNAKE;
}

void ModeSnake::update()
{
	if (pause || !start)
		return;

	unsigned int now = millis();

	if ((double)(now - time) >= 500 * (1.0 / speed))
	{
		if (!this->move())
			this->loose();

		this->display();
		
		time = millis();
	}
}

bool ModeSnake::move()
{
	Point last;
	int i = 0;

	Point next;
	next.x = snake.back().x;
	next.y = snake.back().y;

	if ((dir == UP && lastmove == DOWN) || (dir == DOWN && lastmove == UP) ||
		(dir == LEFT && lastmove == RIGHT) || (dir == RIGHT && lastmove == LEFT))
		dir = lastmove;


	if (dir == UP)
		next.y -= 3;
	else if (dir == DOWN)
		next.y += 3;
	else if (dir == LEFT)
		next.x -= 3;
	else if (dir == RIGHT)
		next.x += 3;
	
	lastmove = dir;

	if (next.x < 0 + 1 || next.x > 126 - 1 || next.y < 0 + 1 || next.y > 44 - 1)
		return false;

	for (int z = 0; z < snake.size(); z++)
		if (snake[z].x == next.x && snake[z].y == next.y)
			return false;
	
	if((next.x == apple.x) && (next.y == apple.y))
	{
		score++;
		speed += 0.1;
		grow(next.x, next.y);
		newapple();
	}
	else
	{
		snake.remove(0);
		snake.push_back(next);
	}

	return true;
}

void ModeSnake::newapple()
{
	int r = random(0, 42 * 15 - snake.size() - 1);
	byte x;
	byte y;

	for (int i = 0; i <= r; i++)
	{
		x = 1 + ((i * 3) % 126);
		y = 1 + 3 * ((i * 3) / 126);

		for (int z = 0; z < snake.size(); z++)
			if (snake[z].x == x && snake[z].y == y)
			{
				r++;
				break;
			}
	}

	apple.x = x;
	apple.y = y;
}

void ModeSnake::grow(byte x, byte y)
{
	if (snake.size() == 254)
		return;

	Point p;

	p.x = x;
	p.y = y;
	snake.push_back(p);
}

void ModeSnake::loose()
{
	lost = true;
	pause = true;

	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->setTextSize(2);
	dsp->println("You lost!");

	dsp->setTextSize(1);
	dsp->println("");
	dsp->print("Score: ");
	dsp->println(score);
	dsp->print("Speed: ");
	dsp->println(speed);
	dsp->println("");
	dsp->println("");
	dsp->println("Press any key to quit");

	dsp->display();
}

void ModeSnake::display_rules()
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->setTextSize(2);
	dsp->println("Snake Game");

	dsp->setTextSize(1);
	dsp->println("8 - up");
	dsp->println("5 - down");
	dsp->println("4 - left");
	dsp->println("6 - right");
	dsp->println("C - pause menu");
	dsp->println("Press any key to play");

	dsp->display();
}

void ModeSnake::display_pause()
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->setTextSize(2);
	dsp->println("Paused");

	dsp->setTextSize(1);
	dsp->println("");
	dsp->print("Score: ");
	dsp->println(score);
	dsp->print("Speed: ");
	dsp->println(speed);
	dsp->println("");
	dsp->println("C - quit");
	dsp->println("= - resume");

	dsp->display();
}

void ModeSnake::display()
{
	if (!start || lost)
		return;
	
	if (pause)
	{
		display_pause();
		return;
	}

	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->setTextSize(1);
	dsp->print("Speed: ");
	dsp->println(speed);
	dsp->print("Score: ");
	dsp->println(score);

	dsp->drawRect(0, 17, 128, 47, WHITE);

	for (int i = 0; i < snake.size(); i++)
	{
		dsp->drawRect(snake[i].x - 1 + 1, snake[i].y - 1 + 18, 3, 3, WHITE);
		dsp->drawPixel(snake[i].x + 1, snake[i].y + 18, WHITE);
	}

	dsp->drawRect(apple.x - 1 + 1, apple.y - 1 + 18, 3, 3, WHITE);

	dsp->display();
}