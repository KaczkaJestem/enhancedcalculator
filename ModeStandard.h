#pragma once
#include "Mode.h"
#include "FileManagement.h"
#include <Float64.h>

class ModeStandard : public Mode
{
	// Keypad map:
	byte keyMap[24];

	// Result string - stores the number displayed on the screen
	String result;

	// Value in the Memory:
	f64 memory;
	// Value of the Operand:
	f64 operand;

	// Indicates if the Operand value is set and ready to use
	bool operand_b;

	// Indicates if the default Result value was modified
	bool result_b;

	// Exponential flag - appears as E on the screen when you exceed the 8-digit limit
	bool exp;

	// Error flag - appears as Err on the screen when an error occurs (e.g. try dividing by 0)
	bool error;

	// ID of the OPERATOR that has been selected:
	byte operator_id;
	
	Settings settings;

	// Indicator used in access code recognition:
	byte code_indicator;

	public:
		ModeStandard(Adafruit_SSD1306* display, SdFat* SD);
		~ModeStandard();

		void display(String str);
		f64 convert(String str);
		String convert(f64 num);

		f64 calculate(f64 operand1, f64 operand2, int operator_id);

		virtual MODE react(byte id);

	private:
		void removeRedundant(String& str);
};