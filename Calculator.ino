#include "GetKey.h"
#include "Utility.h"
#include "ModeStandard.h"
#include "ModeMenu.h"
#include "ModeCodechange.h"
#include "ModeSnake.h"
#include "ModeNotes.h"
#include "ModePlots.h"
#include "ModeChat.h"

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

SdFat sd;
Keypad keypad;
byte lastkey = NO_KEY;

Mode* current_mode;
MODE current_mode_name;

byte keys[24] =
{
	7,8,9,10,
	4,5,6,11,
	1,2,3,12,
	16,0,15,17,

	19,20,21,22,23,13,14,18
};

void setup()
{
	Serial.begin(115200);
	Serial1.begin(115200);

	if (!sd.begin(53, SPI_FULL_SPEED))
		sd.initErrorHalt();

	display.begin(SSD1306_SWITCHCAPVCC, 0x3C);

	display.clearDisplay();
	display.setTextColor(WHITE);
	display.setTextSize(1);
	display.display();

	randomSeed(analogRead(15));

	current_mode = new ModeStandard(&display, &sd);
	current_mode_name = M_STANDARD;
}

void loop()
{
	byte key = GetKey(keypad, lastkey);
	byte id = keys[key - 1];

	if (key != NO_KEY)
	{
		if (current_mode)
		{
			MODE newmode = current_mode->react(id);

			if (newmode != current_mode_name)
			{
				delete current_mode;

				if (newmode == M_STANDARD)
					current_mode = new ModeStandard(&display, &sd);
				else if (newmode == M_MENU)
					current_mode = new ModeMenu(&display, &sd);
				else if (newmode == M_PLOTS)
					current_mode = new ModePlots(&display, &sd);
				else if (newmode == M_NOTES)
					current_mode = new ModeNotes(&display, &sd);
				else if (newmode == M_SNAKE)
					current_mode = new ModeSnake(&display, &sd);
				else if (newmode == M_CHAT)
					current_mode = new ModeChat(&display, &sd);
				else if (newmode == M_CODECHANGE)
					current_mode = new ModeCodechange(&display, &sd);

				current_mode_name = newmode;
			}
		}
	}

	if (current_mode)
		current_mode->update();
	if (Serial1.available())
		handleESPMessage(&sd);
}
