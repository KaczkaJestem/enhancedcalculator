#pragma once
#include <Arduino.h>
#include "FileManagement.h"
#include "ModeChat.h"

int		getFreeRAM();

String	readESP();
void	handleESPMessage(SdFat* sd);
