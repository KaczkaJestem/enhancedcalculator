#include "ModePlots.h"

ModePlots::ModePlots(Adafruit_SSD1306* display, SdFat* SD)
	: Mode(display, SD)
{
	Serial.println("MODE: Plots");

	txt_kbd.setText("", 126);

	time_blinker = 0;

	blinker = false;
	mode = SM_ENTER;

	x = new double;
	expr = NULL;
	cursor = false;

	pos_x = 0;
	pos_y = 0;
	step = 1;
	speed = 0.1;

	this->displayMenu();
}

ModePlots::~ModePlots()
{
	if (expr)
	{
		te_free(expr);
		delete expr;
	}

	delete x;
}

MODE ModePlots::react(byte id)
{
	if (mode == SM_RESULT)
	{
		mode = SM_ENTER;

		this->displayMenu();
		return M_PLOTS;
	}
	else if (mode == SM_PLOT)
	{
		if (id == 23)
		{
			mode = SM_ENTER;
			this->displayMenu();
			return M_PLOTS;
		}
		else if (id == 10)
		{
			cur_pos_x = 63;
			cur_pos_y = 40;
			cursor = !cursor;
		}
		else if (id == 4)
		{
			if (!cursor)
				pos_x -= step * 10;
			else if (cur_pos_x > 0)
				cur_pos_x -= 1;
		}
		else if (id == 6)
		{
			if (!cursor)
				pos_x += step * 10;
			else if (cur_pos_x < 127)
				cur_pos_x += 1;
		}
		else if (id == 2)
		{
			if (!cursor)
				pos_y -= step * 10;
			else if (cur_pos_y > 0)
				cur_pos_y += 1;
		}
		else if (id == 8)
		{
			if (!cursor)
				pos_y += step * 10;
			else if (cur_pos_y < 63)
				cur_pos_y -= 1;
		}
		else if (id == 11 && !cursor)
		{
			double newval = step + speed;
			step = (newval <= 100.0)? newval : 100.0;
		}
		else if (id == 12 && !cursor)
		{
			double newval = step - speed;
			step = (newval >= 0.01)? newval : 0.01;
		}
		else if (id == 19)
		{
			speed = (speed * 10 <= 10.0) ? speed * 10 : 10.0;
		}
		else if (id == 20)
		{
			speed = (speed / 10 >= 0.01) ? speed / 10 : 0.01;
		}

		this->displayPlot();
		return M_PLOTS;
	}
	else if (mode == SM_ENTER)
	{
		if (id == 15 && !txt_kbd.isLiteralModeOn())
		{
			Serial.println(id);
			calculateExpr();
			return M_PLOTS;
		}
		else if (id == 17)
		{
			plotExpr();
			return M_PLOTS;
		}
		else if (id == 23)
		{
			return M_MENU;
		}

		txt_kbd.react(id);
		this->displayMenu();
	}

	return M_PLOTS;
}

void ModePlots::update()
{
	unsigned int now = millis();

	if (mode == SM_ENTER && now - time_blinker > 500)
	{
		blinker = !blinker;
		this->displayMenu();
		time_blinker = now;
	}
}

void ModePlots::displayMenu()
{
	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->println("Enter the expression:");

	if (txt_kbd.isLiteralModeOn())
		dsp->println("[ABC]");
	else
		dsp->println("[123]");

	dsp->print(txt_kbd.getText());

	if (blinker)
	{
		byte caret = txt_kbd.getCaretPos();
		dsp->drawLine(0 + (caret % 21) * 6 , 16 + (caret / 21) * 8, 0 + (caret % 21) * 6, 24 + (caret / 21) * 8, WHITE);
	}

	dsp->display();
}

void ModePlots::calculateExpr()
{
	mode = SM_RESULT;

	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	dsp->println("Result:");
	dsp->println();

	int err = 0;
	f64 result = te_interp(txt_kbd.getText().c_str(), &err);

	if (err != 0)
	{
		dsp->print("Error occured at: ");
		dsp->println(err);
	}
	else
		dsp->println(result);

	dsp->display();
}

void ModePlots::plotExpr()
{
	mode = SM_PLOT;

	te_variable vars[] = { { "x", x } };

	if(expr)
		te_free(expr);

	expr = te_compile(txt_kbd.getText().c_str(), vars, 1, &err);

	this->displayPlot();
}

void ModePlots::displayPlot()
{
	double corner_x = pos_x - 63 * step;
	double corner_y = pos_y - 23 * step;

	dsp->clearDisplay();
	dsp->setCursor(0, 0);

	if (!expr)
	{
		dsp->print("Error ocurred at: ");
		dsp->println(err);

		dsp->display();

		mode = SM_RESULT;
		return;
	}
	else
	{
		if (!cursor)
		{
			dsp->print("At: ");
			dsp->print(pos_x);
			dsp->print(", ");
			dsp->println(pos_y);

			dsp->print("S: ");
			dsp->print(speed * 10);
			dsp->print(" G: ");
			dsp->println(step * 10);
		}
		else
		{
			dsp->print("At: ");
			dsp->print(corner_x + cur_pos_x * step);
			dsp->print(", ");
			dsp->println(corner_y + (47 - cur_pos_y + 16) * step);

			dsp->print("Val: ");

			*x = corner_x + cur_pos_x * step;
			double r = te_eval(expr);

			if (!isnan(r) && !isinf(r))
				dsp->println((f64) r);
			else
				dsp->println("---");
		}
	}

	double axis_x = 40 + pos_y / step;
	double axis_y = 63 - pos_x / step;

	if (axis_x < 16 || axis_x > 63)
		axis_x = 63;

	if (axis_y < 0 || axis_y > 127)
		axis_y = 0;

	// Drawing X axis:
	dsp->drawLine(0, axis_x, 127, axis_x, WHITE);
	// Drawing Y axis:
	dsp->drawLine(axis_y, 16, axis_y, 63, WHITE);

	// Drawing dots on axes:
	double mod_x = fmod(corner_x, step * 10);
	double mod_y = fmod(corner_y, step * 10);

	double tofirstgrad_x = (mod_x >= 0)? (step * 10 - mod_x) / step : abs(mod_x) / step;
	double tofirstgrad_y = (mod_y >= 0)? (step * 10 - mod_y) / step : abs(mod_y) / step;

	for (int i = tofirstgrad_x; i < 128; i += 10)
	{
		dsp->drawPixel(i, axis_x - 1, WHITE);
	}

	for (int i = 63 - tofirstgrad_y; i >= 16; i -= 10)
	{
		dsp->drawPixel(axis_y + 1, i, WHITE);
	}

	if (cursor)
	{
		dsp->drawLine(cur_pos_x, cur_pos_y - 4, cur_pos_x, cur_pos_y - 1, WHITE);
		dsp->drawLine(cur_pos_x, cur_pos_y + 1, cur_pos_x, cur_pos_y + 4, WHITE);
		dsp->drawLine(cur_pos_x - 4, cur_pos_y, cur_pos_x - 1, cur_pos_y, WHITE);
		dsp->drawLine(cur_pos_x + 1, cur_pos_y, cur_pos_x + 4, cur_pos_y, WHITE);
	}
	
	byte lastpoint_x = 255;
	double lastpoint_y;

	for (int i = 0; i < 128; i++)
	{
		// Calculating argument for function:
		*x = (double)i * step - (63.0 - pos_x / step) * step;
		double r = te_eval(expr);

		if (!isnan(r) && !isinf(r))
		{
			// Calculating value for given x:
			double y = 40 + (pos_y - r) / step;
			
			if (y >= 16 && y <= 127)
			{
				if (lastpoint_x != 255)
				{
					if(lastpoint_y < 16)
						dsp->drawLine(lastpoint_x, 16, i, y, WHITE);
					else if(lastpoint_y > 127)
						dsp->drawLine(lastpoint_x, 127, i, y, WHITE);
					else
						dsp->drawLine(lastpoint_x, lastpoint_y, i, y, WHITE);
				}
				else
					dsp->drawPixel(i, y, WHITE);
			}
			else if (lastpoint_x != 255 && lastpoint_y >= 16 && lastpoint_y <= 127)
			{
				if (y < 16)
					dsp->drawLine(lastpoint_x, lastpoint_y, i, 16, WHITE);
				else if (y > 127)
					dsp->drawLine(lastpoint_x, lastpoint_y, i, 127, WHITE);
			}

			lastpoint_x = i;
			lastpoint_y = y;
		}
		else
			lastpoint_x = 255;
	}

	dsp->display();
}