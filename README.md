# Author: Adrian Kiełbowicz

# Enhanced Calculator

A project of a small calculator based on Arduino MEGA2560. The device provides doing simple calculations, storing notes, function plotting and offers communication between several calculators.
This repo contains the software code for Arduino.

## Calculator provides two main modes:

* **Simple Mode**, in which user can perform only basic math operations like on the standard calculator device
* **Enhanced Mode**, in which user can access a menu of hidden options, like evaluation of math expressions, function plotting, reading notes from SD card, communicating with other devices (creating a new chatroom or joining an existing one) or even playing the classical Snake game.

The hidden menu pops up only if user pressed '+' button 2 times and then entered a proper 4-digit passcode; the passcode can be modified from the options menu in the enhanced mode.

**Note: This software uses additional dependencies:**

* TinyExpr math parser: https://github.com/codeplea/tinyexpr
* Adafruit GFX: https://github.com/adafruit/Adafruit-GFX-Library
* Adafruit SSD1306: https://github.com/adafruit/Adafruit_SSD1306
* Keypad: https://github.com/Chris--A/Keypad
* Vector: https://github.com/janelia-arduino/Vector
* SdFat: https://github.com/greiman/SdFat
* Float64: https://github.com/mmoller2k/Float64


## Device construction

The device consists of:

* Arduino Mega2560 Microcontroller
* 16-button Arduino keypad
* 2 x 4-button keypads (soldered by me this time, but probably should be replaced with some mass-produced for convenience)
* ESP8266-01 module to provide communication system
* SD card reader
* Small 128 x 64 OLED display 

Moveover, it should be powered by 9V battery.
