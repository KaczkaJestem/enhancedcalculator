#pragma once
#include "Utility.h"
#include "Mode.h"
#include "FileManagement.h"
#include "TextKeyboard.h"

class ModeChat : public Mode
{
private:
	struct Session
	{
		bool available = false;
		String name;
		byte client_count;
		unsigned int lines_count;
	};

	enum SUBMODE
	{
		SM_ERROR,
		SM_NAMESETUP,
		SM_CHAT,
		SM_BROWSER
	};

	SUBMODE mode;
	byte keyMap[24];

	TextKeyboard txt_kbd;

	uint16_t chatline_offset;
	int saveindex;

	unsigned int time_blinker;
	bool blinker;


	String server_list;
	int browsing_index;
	int servers_count;
	byte browser_page;

public:
	static Session session;

	ModeChat(Adafruit_SSD1306* display, SdFat* SD);
	~ModeChat();

	virtual MODE react(byte id);
	virtual void update();

	bool setupServer();
	bool setupClient(String ap_name);

	void sendMessage(String data);
	void getAPs();

	void exit();
	bool reset(bool quiet);
	
	void displayError(String err);
	void displaySetup();
	void displayBrowser();
	void displayChat();
	void displayWaitScreen(String info);
};